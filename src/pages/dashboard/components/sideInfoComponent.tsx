import React from 'react';
import { Typography, Button } from 'antd';
import './sideInfoComponent.less';

const { Title } = Typography;

const SideInfoComponent: React.FC = () => {
  return (
    <div className="sideInfo-container">
      <Title level={4}>Usage</Title>
      <Button type="primary" shape="round" style={{ backgroundColor: '#8090A0' }}>
        Widget Code
      </Button>
      <Title level={4}>Integrations</Title>
      <p>None</p>
      <Button type="primary" shape="round">
        Connect
      </Button>
    </div>
  );
};
//
export default SideInfoComponent;
