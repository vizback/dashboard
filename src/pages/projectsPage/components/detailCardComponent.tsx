import React from 'react';
import './detailCard.less';
import { Button } from 'antd';
import { SettingOutlined, CopyOutlined, DownloadOutlined, DeleteOutlined } from '@ant-design/icons';

const DetailCard: React.FC = () => {
  return (
    <div className="detail-box-container">
      <div className="project-detail-box">
        <div className="project-name">Project Name</div>
        <div className="feedback-info">Feedback info</div>
        <Button type="primary" style={{ borderRadius: '20px' }}>
          View Project
        </Button>
      </div>
      <div className="box-footer">
        <div className="box-footer-left">H</div>
        <div className="box-footer-right">
          <Button className="setting-btn">
            <SettingOutlined />
          </Button>
          <Button className="copy-btn">
            <CopyOutlined />
          </Button>
          <Button className="archive-btn">
            <DownloadOutlined />
          </Button>
          <Button className="delete-btn">
            <DeleteOutlined />
          </Button>
        </div>
      </div>
    </div>
  );
};

export default DetailCard;
