import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import AccountPageNavbar from '@/components/accountNavBar';
import './dashboardLayout.less';

const AccountPageLayout: React.FC<{}> = ({ children }) => {
  return (
    <PageContainer pageHeaderRender={() => null}>
      <AccountPageNavbar />
      {children}
    </PageContainer>
  );
};

export default AccountPageLayout;
