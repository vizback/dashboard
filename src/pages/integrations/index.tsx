import React from 'react';
import { Card, Col, Row, Checkbox, Button, Divider } from 'antd';
import './index.less';
import slack from './slack-2.svg';
import microsoftteams from './teams.png';
import jira from './Jira-new-logo.png';
import teamwork from './teamwork.png';
import trello from './Trello-Logo.wine.svg';
import basecamp from './bascamp.jpeg';
import asana from './asana.png';
import clickup from './ClickUP_Logo.jpg';
import monday from './Monday.com-Logo.wine.svg';
import github from './github.png';
import gitlab from './GitLab-Logo.wine.svg';
import webhook from './webhook.png';
import wordpress from './wordpress.png';

const Integrations: React.FC = () => {
  return (
    <>
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={slack} alt="slack" className="logo" />
              <br />
              <br />
              <br />
              <p>Send messages to my Slack channel when</p>
              <Checkbox className="black">Feedback is created</Checkbox>
              <br />
              <Checkbox className="black">Feedback is assigned</Checkbox>
              <br />
              <Checkbox className="black">Feedback status is changed</Checkbox>
              <br />
              <Checkbox className="black">New comments are added to a feedback</Checkbox>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={microsoftteams} alt="microsoft teams" className="logo" />
              <br />
              <br />
              <br />
              <p>Send messages to Microsoft Teams when</p>
              <Checkbox className="black">Feedback is created</Checkbox>
              <br />
              <Checkbox className="black">Feedback is assigned</Checkbox>
              <br />
              <Checkbox className="black">Feedback status is changed</Checkbox>
              <br />
              <Checkbox className="black">New comments are added to a feedback</Checkbox>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={jira} alt="jira" className="logo" />
              <br />
              <br />
              <br />
              <p>Create issues in JIRA when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
        </Row>
      </div>
      <br />
      <br />
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={teamwork} alt="teamwork" className="logo" />
              <br />
              <br />
              <br />
              <p>Create tasks in Teamwork when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={trello} alt="trello" className="logo" />
              <br />
              <br />
              <br />
              <p>Create cards in Trello when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={basecamp} alt="basecamp" className="logo" />
              <br />
              <br />
              <br />
              <p>Create to-dos in Basecamp when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
        </Row>
      </div>
      <br />
      <br />
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={asana} alt="asana" className="logo" />
              <br />
              <br />
              <br />
              <p>Create tasks in Asana when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={clickup} alt="clickup" className="logo" />
              <br />
              <br />
              <br />
              <p>Create tasks in ClickUp when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={monday} alt="monday.com" className="logo" />
              <br />
              <br />
              <br />
              <p>Create items in Monday.com when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
        </Row>
      </div>
      <br />
      <br />
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={github} alt="github" className="logo" />
              <br />
              <br />
              <br />
              <p>Create issues in GitHub when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={gitlab} alt="gitlab" className="logo" />
              <br />
              <br />
              <br />
              <p>Create issues in GitLab when Feedback is created.</p>
              <p>Click the Activate button below to get started.</p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={webhook} alt="webhook" className="logo" />
              <br />
              <br />
              <br />
              <p>
                Every time a visitor gives feedback, we can POST all the data of the feedback item
                in JSON format to a URL you specify.
              </p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
        </Row>
      </div>
      <br />
      <br />
      <div className="site-card-wrapper">
        <Row gutter={16}>
          <Col span={8}>
            <Card style={{ width: 350 }}>
              <img src={wordpress} alt="wordpress" className="logo" />
              <br />
              <br />
              <br />
              <p>
                Get feedback from your WordPress website with screenshots from Userback. Download
                the plugin to enable Userback on specific, all or draft pages only.
              </p>
              <br />
              <br />
              <Divider />
              <Button type="primary" shape="round" size="large" className="button" block>
                Activate
              </Button>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Integrations;
