import React from 'react';
import { Typography, Row, Col, Badge } from 'antd';
import './peopleComponent.less';

const { Title } = Typography;

const PeopleDetail: React.FC = () => {
  return (
    <div>
      <Title level={4}>People</Title>
      <div>
        <Row>
          <Col span={24} className="unassigned-people-detail box-style">
            <div className="unassigned-count">
              <Badge count={0} style={{ backgroundColor: '#7828f0', width: '36px' }} showZero />
            </div>
            <div>
              <b>Unassigned</b>
            </div>
          </Col>
          <Col span={24} className="assigned-people-detail box-style">
            <div className="assigned-count">
              <Badge count={0} style={{ backgroundColor: '#7828f0', width: '36px' }} showZero />
            </div>
            <div>
              <b>Username</b>
              <p>User description</p>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default PeopleDetail;
