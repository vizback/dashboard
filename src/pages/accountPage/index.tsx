import React from 'react';
import AccountDetails from './components/accountDetails';
import AccountSummary from './components/accountSummary';
import { Row, Col } from 'antd';
import './index.less';

const AccountOverview: React.FC = () => {
  return (
    <div className="account-page-container">
      <Row gutter={24}>
        <Col xs={24} sm={24} md={12} lg={14} xl={14} className="guttter-row">
          <AccountDetails />
        </Col>
        <Col xs={24} sm={24} md={12} lg={10} xl={10} className="guttter-row">
          <AccountSummary />
        </Col>
      </Row>
    </div>
  );
};

export default AccountOverview;
