import React from 'react';
import './projectDetailComponent.less';
import SearchBar from './projectSearchBar';
import ActiveProjects from './activeProjects';
import ArchivedProjects from './archivedProjects';

const ProjectDetail: React.FC = () => {
  return (
    <div className="project-detail-container">
      <SearchBar />
      <ActiveProjects />
      <ArchivedProjects />
    </div>
  );
};

export default ProjectDetail;
